---
title: "Manifesting preferences.org.au"
author: "Fraser Patterson"
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
output:
  bookdown::gitbook:
    config:
      fontsettings:
        theme: night
        family: sans
        size: 2
      edit : https://gitlab.com/snuggy/preferences-book/%s
    includes:
      in_header: header.html
    fig_caption: false
  pdf_document:
    includes:
      in_header: preamble.tex
      toc: true
      number_sections: true
      fig_caption: false
      latex_engine: xelatex

documentclass: book
bibliography: [book.bib, packages.bib]
biblio-style: apalike
link-citations: yes
url: 'https\://book.preferences.org.au/'
cover-image: ""
apple-touch-icon: "touch-icon.png"
apple-touch-icon-size: 120
favicon: "favicon.ico"
description: "Work in progress book on making preferences.org.au a thing"
---

# Preface {-}

You are reading an open source, frequently updated book on making teams to educate people about minor parties, written by Fraser Patterson of preferences.org.au.

We want mainly minor parties elected, with few people voting for the two majors. We'll have a fairer democracy and propsperity. It's possible, provided we work together.

Most people vote major party for two reasons:

1. They were never taught (by our government) how voting works
2. They're unsure what the minor parties are about.

We'll create mass education campaigns with independent summaries of minor parties to counter this.

We need to influence as many people as possible before the next election, held at an unknown date before the XX of XXXX 2022. We need to constantly grow -- seeing preferences references must be common. We must be very open, and anyone should be able to help out. All unique skillsets are used, so think of how you can make a difference with people you care about. We only get one shot; this election will influence our future forever.

Forming a team is ideal. Many things must be done within a limited timeframe, and we work better in teams. Individuals can help out in many, many ways with little coodination (and are needed) but working with others makes it more fun. Contact people within your state / city / electorate on Urbit and work together. This book is a live document on what we're learning.

## Book summary

Urgently but comfortably (preferably) create a team to spread the message, distribute resources to mailboxes, XXXXX , XXXXX, and XXXXX. Anyone must be able to take easily take individual action to support the cause. We all must be transparent, honest and independent. You must be committed and willing to run or work in happy teams. Cultivate kindness and joy, remember that everyone is human. We're not against any one person. Countries get the governments they deserve, so we better work hard.

Here's what it discusses:

An overview
	The problem
	What doing?
		Mass marketing campaign
		Events Urgency
	Values / What we believe


What are we trying to do?
	Defining everything
	How we're going to solve it
		Marketing campaigns
			Digitial
				Social media
				Urbit
			Physical mediums
				Self-serve advertising
				Print media
				Posters
				DIY jobs
				Wacko schemes
		Events
			Publicity
				Calm

		Helping out

How we'll do it
	Values
		Kind
		Simple
		Effective
	Ideas Man
		Your solutions
			free thinking exercise to make this happen
			stunts
		Developing software
			gps
			urbit
			bot
	Teamwork
		Team formation
			Theroy
			Values
		Accomplishing stuff as a team

Nitty gritty
	Tools we're using
		Basecamp
		Urbit
	Writing
		Simple style
		Australian government style guide
	Communications
		Good writing
		Calm
		Respectful

Heaps about about making teams
	Lots on team foundation
		link into values
	Different roles

Self-cultivation
	Sleep
	Blood donations


## Before we get into it

Thanks for taking the time
